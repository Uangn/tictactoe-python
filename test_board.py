from game import *

def test(g: Game) -> tuple[Game, GameState]:
    """Run test game, give back it's final state"""
    while True:
        print_board(g.board)

        # Not valid input
        if g.make_move(Move(int(input())-1,int(input())-1)):
            state = g.game_state()

            if not state == GameState.Running:
                return (g, state) # Final state

def print_board(b):
    for r in b:
        print(*r)

def test_with_print(g: Game) -> None:
    """Run test game, and print final state message"""
    final_game, game_state = test(g)
    match game_state:
        case GameState.WinX:
            print_board(final_game.board)
            print(f"Player.X wins")

        case GameState.WinO:
            print_board(final_game.board)
            print(f"Player.O wins")

        case GameState.Draw:
            print_board(final_game.board)
            print("Draw")

test_with_print(Game())
