from enum import Enum
from functools import reduce
from collections import namedtuple

Move = namedtuple('Move', ['x', 'y'])

Player = Enum('Player', ['X','O'])

GameState = Enum('GameState', ['WinX', 'WinO', 'Draw', 'Running'])

Diagonal = Enum('Diagonals', ['TL_BR', 'TR_BL'])

BOARD_SIZE = 3

class Game():
    def __init__(self, 
                 init_player: Player = Player.X,
                 init_board: list[list[Player]] = None) -> None:
        self.board = init_board or self.init_board()
        self.player = init_player

    def init_board(self) -> list[list[int]]:
        """Returns a cleared board"""
        return [[None for _ in range(BOARD_SIZE)] for _ in range(BOARD_SIZE)][:]

    def valid_move(self, move: Move) -> bool:
        """Returns whether that spot is empty or not"""
        return self.board[move.x][move.y] == None

    def mod_board(self, move: Move):
        """Returns self: board at the move position as current player"""
        self.board[move.x][move.y] = self.player
        return self

    def next_player(self):
        """Returns the opposite player enum"""
        self.player = Player.X if self.player == Player.O else Player.O
        return self

    def make_move(self, move: Move):
        """
        if the move is valid
            return self: board at that position, swaps player
        """
        if self.valid_move(move):
            return self.mod_board(move).next_player()
        else:
            return None


    def game_state(self) -> GameState:
        """Returns the current state of the board"""
        for i in range(BOARD_SIZE):
            #Horizontal Check
            if self.row_has_player(i, Player.X):
                return GameState.WinX
            if self.row_has_player(i, Player.O):
                return GameState.WinO

            #Vertical Check
            if self.col_has_player(i, Player.X):
                return GameState.WinX
            if self.col_has_player(i, Player.O):
                return GameState.WinO

        #Diagonal Check
        if self.diag_has_player(Diagonal.TL_BR, Player.X):
            return GameState.WinX
        if self.diag_has_player(Diagonal.TL_BR, Player.O):
            return GameState.WinO

        if self.diag_has_player(Diagonal.TR_BL, Player.X):
            return GameState.WinX
        if self.diag_has_player(Diagonal.TR_BL, Player.O):
            return GameState.WinO

        # All are placed
        if reduce(lambda x,y: x and all(y), self.board, True):
            return GameState.Draw
        else:
            return GameState.Running

        # #Running
        # for row in self.board:
        #     for sect in row:
        #         if (sect == None):
        #             return GameState.Running
        # #Draw
        # return GameState.Draw


    def row_has_player(self, r: int, p: Player) -> bool:
        return self.board[r][0] == self.board[r][1] == self.board[r][2] == p
    
    def col_has_player(self, c: int, p: Player) -> bool:
        return self.board[0][c] == self.board[1][c] == self.board[2][c] == p
    
    def diag_has_player(self, d: Diagonal, p: Player) -> bool:
        if d == Diagonal.TL_BR:
            return self.board[0][0] == self.board[1][1] == self.board[2][2] == p
        else:
            return self.board[0][2] == self.board[1][1] == self.board[2][0] == p

        

